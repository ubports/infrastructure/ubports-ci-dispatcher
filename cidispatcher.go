// Copyright (C) 2021 Guido Berhoerster <guido+ubports@berhoerster.name>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package cidispatcher

import (
	"context"
	"crypto/rand"
	"crypto/subtle"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path"
	"regexp"
	"strings"
	"sync"
	"syscall"
	"time"
)

const (
	MaxConcurrentJobs = 4
)

var (
	branchName = regexp.MustCompile(`^ubports/([a-z]+(?:_-_.+)?)$`)
	jobName    = regexp.MustCompile(`^build-package-([a-z]+)$`)
)

func urlJoinPath(baseURL, p string) string {
	u, err := url.Parse(baseURL)
	if err != nil {
		u, _ = url.Parse("/")
	}
	u.Path = path.Join(u.Path, p)
	return u.String()
}

func randomToken() string {
	var token [16]byte
	if _, err := rand.Read(token[:]); err != nil {
		panic(err)
	}
	return fmt.Sprintf("%x", token)
}

func repoName(buildName, ref string) string {
	var repo string
	if ref == "master" || ref == "main" {
		// get repo from build job name
		m := jobName.FindStringSubmatch(buildName)
		if m == nil {
			return ""
		}
		repo = m[1]
	} else {
		m := branchName.FindStringSubmatch(ref)
		if m == nil {
			return ""
		}
		repo = m[1]
	}
	return strings.ReplaceAll(repo, "/", "_")
}

type PipelineEvent struct {
	ObjectKind  string `json:"object_kind"`
	ObjectAttrs struct {
		Status string `json:"status"`
		Ref    string `json:"ref"`
	} `json:"object_attributes"`
	Project struct {
		ID   uint64 `json:"id"`
		Name string `json:"path_with_namespace"`
	} `json:"project"`
	Builds []struct {
		Status        string `json:"status"`
		Name          string `json:"name"`
		ID            uint64 `json:"id"`
		ArtifactsFile struct {
			Filename string `json:"filename"`
		} `json:"artifacts_file"`
	} `json:"builds"`
}

func parsePipelineEvent(r io.Reader) (*PipelineEvent, error) {
	var pe *PipelineEvent
	if err := json.NewDecoder(r).Decode(&pe); err != nil {
		return nil, err
	}
	if pe.ObjectKind != "pipeline" || pe.ObjectAttrs.Ref == "" ||
		pe.Project.Name == "" {
		return nil, errors.New("unexpected data format")
	}
	return pe, nil
}

type PublishBuildJob struct {
	ID        string
	ProjectID string
	BuildID   string
	Repo      string
	Filename  string
}

type CIDispatcher struct {
	srv     *http.Server
	log     Logger
	uploadc chan PublishBuildJob
	config  *Config
}

func NewCIDispatcher(c *Config, l Logger) *CIDispatcher {
	d := &CIDispatcher{
		log:    l,
		config: c,
	}

	mux := http.NewServeMux()
	mux.Handle("/", d.webhookHandlerFunc())

	d.srv = &http.Server{
		Addr:           c.Addr,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
		Handler:        mux,
	}

	return d
}

func (d *CIDispatcher) webhookHandlerFunc() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		d.log.Debugf("request: %s %s %s", r.RemoteAddr, r.Method, r.URL)
		if r.URL.Path != "/gitlab-webhook" {
			http.NotFound(w, r)
			return
		}

		if r.Method != http.MethodPost {
			http.Error(w,
				http.StatusText(http.StatusMethodNotAllowed),
				http.StatusMethodNotAllowed)
			return
		}

		ht := r.Header.Get("X-Gitlab-Token")
		var n int
		for _, t := range d.config.Tokens {
			n += subtle.ConstantTimeCompare([]byte(t), []byte(ht))
		}
		if n == 0 {
			d.log.Debugf("invalid or missing X-Gitlab-Token")
			http.Error(w,
				http.StatusText(http.StatusForbidden),
				http.StatusForbidden)
			return
		}

		if r.Header.Get("X-Gitlab-Event") != "Pipeline Hook" {
			d.log.Debugf("request is not a pipeline event")
			w.WriteHeader(http.StatusNoContent)
			return
		}

		pe, err := parsePipelineEvent(r.Body)
		if err != nil {
			d.log.Printf("failed to parse pipeline event: %s", err)
			http.Error(w, http.StatusText(http.StatusBadRequest),
				http.StatusBadRequest)
			return
		}
		if pe.ObjectAttrs.Status != "success" {
			d.log.Debugf("pipeline did not complete successfully")
			w.WriteHeader(http.StatusNoContent)
			return
		}
		if pe.ObjectAttrs.Ref != "master" &&
			pe.ObjectAttrs.Ref != "main" &&
			!branchName.MatchString(pe.ObjectAttrs.Ref) {
			d.log.Debugf("pipeline event for ref %s ignored",
				pe.ObjectAttrs.Ref)
			w.WriteHeader(http.StatusNoContent)
			return
		}

		for _, b := range pe.Builds {
			if b.Status != "success" {
				d.log.Debugf("build %d did not complete successfully",
					b.ID)
				continue
			}

			if !jobName.MatchString(b.Name) {
				d.log.Debugf("build %d job %s does not match",
					b.ID, b.Name)
				continue
			}

			repo := repoName(b.Name, pe.ObjectAttrs.Ref)
			if repo == "" {
				d.log.Debugf("could not determine repository name of build %d",
					b.ID)
				continue
			}

			if b.ArtifactsFile.Filename == "" {
				d.log.Debugf("build %d has no build artifacts",
					b.ID)
				continue
			}

			job := PublishBuildJob{
				ID:        randomToken(),
				ProjectID: fmt.Sprintf("%d", pe.Project.ID),
				BuildID:   fmt.Sprintf("%d", b.ID),
				Repo:      repo,
				Filename:  b.ArtifactsFile.Filename,
			}
			d.log.Printf("[%s] project %s (%s), completed build %s of %q sucessfully",
				job.ID, job.ProjectID, pe.Project.Name, job.BuildID,
				pe.ObjectAttrs.Ref)
			select {
			case d.uploadc <- job:
			default:
				d.log.Printf("[%s] failed to queue upload job: queue full",
					job.ID)
				http.Error(w,
					http.StatusText(http.StatusInternalServerError),
					http.StatusInternalServerError)
				return
			}
		}

		w.WriteHeader(http.StatusNoContent)
	})
}

func (d *CIDispatcher) ListenAndServe() error {
	d.log.Printf("starting server, listening on %s", d.srv.Addr)

	d.uploadc = make(chan PublishBuildJob, MaxConcurrentJobs*4)
	publishc := make(chan PublishBuildJob, MaxConcurrentJobs*4)

	gitLab := NewGitLabClient(d.config.GitLabURL)
	aptly := NewAptlyClient(d.config.AptlyURL)

	p := NewPublisher(publishc, aptly, d.log, d.config)
	publisherComplete := make(chan struct{})
	go func() {
		defer close(publisherComplete)
		p.Run()
	}()

	wg := sync.WaitGroup{}
	shutdownComplete := make(chan struct{})
	go func() {
		signalReceived := make(chan os.Signal, 1)
		signal.Notify(signalReceived, os.Interrupt, syscall.SIGTERM)
		s := <-signalReceived
		d.log.Printf("received signal %d, shutting down", s)

		ctx, cancel := context.WithTimeout(context.Background(),
			10*time.Second)
		if err := d.srv.Shutdown(ctx); err != nil {
			d.log.Printf("failed to shut down server: %s", err)
		}
		cancel()

		close(d.uploadc)
		// drain queue, wait for uploaders to finish
		for range d.uploadc {
		}
		wg.Wait()

		close(publishc)
		// wait for publisher to finish
		<-publisherComplete

		close(shutdownComplete)
	}()

	for i := 0; i < MaxConcurrentJobs; i++ {
		u := NewUploader(d.uploadc, publishc, gitLab, aptly, d.log,
			d.config)
		wg.Add(1)
		go func() {
			defer wg.Done()
			u.Run()
		}()
	}

	if err := d.srv.ListenAndServe(); err != http.ErrServerClosed {
		return fmt.Errorf("failed to start server: %w", err)
	}
	<-shutdownComplete
	d.log.Printf("server was shut down")

	return nil
}
