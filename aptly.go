// Copyright (C) 2021 Guido Berhoerster <guido+ubports@berhoerster.name>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package cidispatcher

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

var (
	ErrRepoExists             = errors.New("repository already exists")
	ErrPublishedRepoExists    = errors.New("published repository already exists")
	ErrPublishedRepoNotExists = errors.New("published repository does not exist")
)

type AptlyClient struct {
	baseURL string
	c       *http.Client
}

func NewAptlyClient(baseURL string) *AptlyClient {
	return &AptlyClient{
		baseURL: baseURL,
		c:       &http.Client{Timeout: 60 * time.Second},
	}
}

func (ac *AptlyClient) CreateRepo(ctx context.Context, name string) error {
	payload, _ := json.Marshal(struct{ Name string }{name})
	req, err := http.NewRequest(http.MethodPost,
		urlJoinPath(ac.baseURL, "/repos"), bytes.NewReader(payload))
	if err != nil {
		return err
	}
	req = req.WithContext(ctx)
	req.Header.Set("Content-Type", "application/json")
	resp, err := ac.c.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode == http.StatusBadRequest {
		return ErrRepoExists
	} else if resp.StatusCode != http.StatusOK {
		return err
	}
	return nil
}

func (ac *AptlyClient) uploadFiles(ctx context.Context, uploadDir string, fsys fs.FS, filenames []string) error {
	if len(filenames) == 0 {
		return errors.New("no packages to upload")
	}

	r, w := io.Pipe()
	defer r.Close()
	mpw := multipart.NewWriter(w)
	errc := make(chan error, 1)
	go func() {
		defer func() {
			cErr := mpw.Close()
			select {
			case errc <- cErr:
			}
			w.Close()
		}()

		for _, filename := range filenames {
			part, err := mpw.CreateFormFile("file",
				path.Base(filename))
			if err != nil {
				errc <- err
				return
			}
			f, err := fsys.Open(filename)
			if err != nil {
				errc <- err
				return
			}
			_, err = io.Copy(part, f)
			f.Close()
			if err != nil {
				errc <- err
				return
			}
		}
	}()
	u := urlJoinPath(ac.baseURL, fmt.Sprintf("/files/%s",
		url.PathEscape(uploadDir)))

	req, err := http.NewRequest(http.MethodPost, u, r)
	if err != nil {
		return err
	}
	req = req.WithContext(ctx)
	req.Header.Set("Content-Type", mpw.FormDataContentType())
	resp, err := ac.c.Do(req)
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected HTTP status: %s", resp.Status)
	}

	if err := <-errc; err != nil {
		return err
	}

	return nil
}

func (ac *AptlyClient) UploadFiles(ctx context.Context, uploadDir string, filenames ...string) error {
	for i, filename := range filenames {
		absFilename, err := filepath.Abs(filename)
		if err != nil {
			return err
		}
		filenames[i] = absFilename
	}
	return ac.uploadFiles(ctx, uploadDir, os.DirFS("/"), filenames)
}

func (ac *AptlyClient) UploadFS(ctx context.Context, uploadDir string, fsys fs.FS) error {
	filenames := []string{}
	fs.WalkDir(fsys, ".", func(p string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if d.IsDir() {
			return nil
		}
		if ext := path.Ext(d.Name()); ext == ".deb" || ext == ".udeb" {
			filenames = append(filenames, p)
		}
		return nil
	})
	return ac.uploadFiles(ctx, uploadDir, fsys, filenames)
}

func (ac *AptlyClient) AddToRepo(ctx context.Context, name, uploadDir string) error {
	u := urlJoinPath(ac.baseURL, fmt.Sprintf("/repos/%s/file/%s",
		url.PathEscape(name), url.PathEscape(uploadDir)))
	data := url.Values{"forceReplace": {"1"}}
	req, err := http.NewRequest(http.MethodPost, u,
		strings.NewReader(data.Encode()))
	if err != nil {
		return err
	}
	req = req.WithContext(ctx)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := ac.c.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected HTTP status: %s", resp.Status)
	}
	return nil
}

func (ac *AptlyClient) PublishRepo(ctx context.Context, distro, gpgKey, gpgPassphraseFile string) error {
	payload, _ := json.Marshal(map[string]interface{}{
		"SourceKind": "local",
		"Sources": []map[string]interface{}{
			map[string]interface{}{
				"Name": distro,
			},
		},
		"Distribution": distro,
		"Signing": map[string]interface{}{
			"PassphraseFile": gpgPassphraseFile,
		},
	})
	req, err := http.NewRequest(http.MethodPost,
		urlJoinPath(ac.baseURL, "/publish"), bytes.NewReader(payload))
	if err != nil {
		return err
	}
	req = req.WithContext(ctx)
	req.Header.Set("Content-Type", "application/json")
	resp, err := ac.c.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode == http.StatusBadRequest {
		return ErrPublishedRepoExists
	} else if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("Unknown HTTP error: %s", resp.Status)
	}
	return nil
}

func (ac *AptlyClient) UpdateRepo(ctx context.Context, distro, gpgKey, gpgPassphraseFile string) error {
	payload, _ := json.Marshal(map[string]interface{}{
		"Signing": map[string]interface{}{
			"GpgKey":         gpgKey,
			"PassphraseFile": gpgPassphraseFile,
		},
	})
	u := urlJoinPath(ac.baseURL, fmt.Sprintf("/publish/:./%s",
		url.PathEscape(distro)))
	req, err := http.NewRequest(http.MethodPut, u, bytes.NewReader(payload))
	if err != nil {
		return err
	}
	req = req.WithContext(ctx)
	req.Header.Set("Content-Type", "application/json")
	resp, err := ac.c.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode == http.StatusNotFound {
		return ErrPublishedRepoNotExists
	} else if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Unknown HTTP error: %s", resp.Status)
	}
	return nil
}

func (ac *AptlyClient) PublishOrUpdateRepo(ctx context.Context, distro, gpgKey, gpgPassphraseFile string) error {
	err := ac.PublishRepo(ctx, distro, gpgKey, gpgPassphraseFile)
	if errors.Is(err, ErrPublishedRepoExists) {
		return ac.UpdateRepo(ctx, distro, gpgKey, gpgPassphraseFile)
	}
	return err
}
