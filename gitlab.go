// Copyright (C) 2021 Guido Berhoerster <guido+ubports@berhoerster.name>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package cidispatcher

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"
	"context"
)

type GitLabClient struct {
	baseURL string
	c       *http.Client
}

func NewGitLabClient(baseURL string) *GitLabClient {
	return &GitLabClient{
		baseURL: baseURL,
		c:       &http.Client{Timeout: 60 * time.Second},
	}
}

func (glc *GitLabClient) DownloadArtifacts(ctx context.Context, projectID, buildID string, w io.Writer) error {
	u := urlJoinPath(glc.baseURL,
		fmt.Sprintf("/projects/%s/jobs/%s/artifacts",
			url.PathEscape(projectID), url.PathEscape(buildID)))
	req, err := http.NewRequest(http.MethodGet, u, nil)
	if err != nil {
		return err
	}
	req = req.WithContext(ctx)
	resp, err := glc.c.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected HTTP status: %s", resp.Status)
	}

	contentType := resp.Header.Get("Content-Type")
	if contentType != "application/zip" &&
		contentType != "application/octet-stream" {
		return fmt.Errorf("unexpected content type %q", contentType)
	}

	_, err = io.Copy(w, resp.Body)
	return err
}
