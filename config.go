// Copyright (C) 2021 Guido Berhoerster <guido+ubports@berhoerster.name>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package cidispatcher

import (
	"fmt"
	"net/url"
	"os"

	"github.com/pelletier/go-toml/v2"
)

const (
	DefaultRepo      = "ubports-ci"
	DefaultGitLabURL = "https://gitlab.com/api/v4"
	DefaultAptlyURL  = "http://localhost:8080/api"
)

type Config struct {
	Addr              string   `toml:"listen_address"`
	Tokens            []string `toml:"tokens"`
	IncomingPath      string   `toml:"incoming_path"`
	GitLabURL         string   `toml:"gitlab_url"`
	AptlyURL          string   `toml:"aptly_url"`
	GPGKey            string   `toml:"gpg_key"`
	GPGPassphraseFile string   `toml:"gpg_passphrase_file"`
	Logfile           string   `toml:"logfile"`
}

func ReadConfig(configFile string) (*Config, error) {
	c := &Config{}
	f, err := os.Open(configFile)
	if err != nil {
		return nil,
			fmt.Errorf("failed to open configuration file: %w", err)
	}
	defer f.Close()
	if err := toml.NewDecoder(f).Decode(c); err != nil {
		return nil, fmt.Errorf("failed to read configuration file: %w",
			err)
	}

	if c.Addr == "" {
		c.Addr = ":4040"
	}

	if len(c.Tokens) == 0 {
		return nil,
			fmt.Errorf("configuration error: no tokens specified")
	}

	if c.IncomingPath == "" {
		c.IncomingPath = "./incoming"
	}

	if c.GitLabURL == "" {
		c.GitLabURL = DefaultGitLabURL
	}
	if _, err = url.Parse(c.GitLabURL); err != nil {
		return nil, fmt.Errorf("configuration error: invalid URL: %w",
			err)
	}

	if c.AptlyURL == "" {
		c.AptlyURL = DefaultAptlyURL
	}
	if _, err = url.Parse(c.AptlyURL); err != nil {
		return nil, fmt.Errorf("configuration error: invalid URL: %w",
			err)
	}

	return c, nil
}
