// Copyright (C) 2021 Guido Berhoerster <guido+ubports@berhoerster.name>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package cidispatcher

import (
	"archive/zip"
	"os"
	"path/filepath"
	"context"
	"time"
)

type Uploader struct {
	uploadc  <-chan PublishBuildJob
	publishc chan<- PublishBuildJob
	gitLab   *GitLabClient
	aptly    *AptlyClient
	log      Logger
	config   *Config
}

func NewUploader(uploadc <-chan PublishBuildJob, publishc chan<- PublishBuildJob, gitLab *GitLabClient, aptly *AptlyClient, l Logger, config *Config) *Uploader {
	return &Uploader{
		uploadc:  uploadc,
		publishc: publishc,
		gitLab:   gitLab,
		aptly:    aptly,
		log:      l,
		config:   config,
	}
}

func (u *Uploader) downloadArtifacts(ctx context.Context, job PublishBuildJob, filename string) error {
	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	err = u.gitLab.DownloadArtifacts(ctx, job.ProjectID, job.BuildID, f)
	cerr := f.Close()
	if err == nil {
		err = cerr
	}
	return err
}

func (u *Uploader) uploadFiles(ctx context.Context, job PublishBuildJob, filename string) error {
	r, err := zip.OpenReader(filename)
	if err != nil {
		return err
	}
	defer r.Close()
	return u.aptly.UploadFS(ctx, job.ID, r)
}

func (u *Uploader) processJob(job PublishBuildJob) {
	ctx, cancel := context.WithTimeout(context.Background(),
		120*time.Second)
	defer cancel()

	u.log.Debugf("[%s] downloading artifacts from GitLab", job.ID)
	filename := filepath.Join(u.config.IncomingPath,
		job.ID+filepath.Ext(job.Filename))
	if err := u.downloadArtifacts(ctx, job, filename); err != nil {
		u.log.Printf("[%s] download failed: %s", job.ID, err)
		return
	}

	u.log.Debugf("[%s] uploading packages to Aptly", job.ID)
	if err := u.uploadFiles(ctx, job, filename); err != nil {
		u.log.Printf("[%s] upload failed: %s", job.ID, err)
		return
	}

	u.publishc <- job

	// keep artifacts in case of errors
	os.Remove(filename)
}

func (u *Uploader) Run() {
	for job := range u.uploadc {
		u.processJob(job)
	}
}
