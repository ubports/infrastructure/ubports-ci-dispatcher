// Copyright (C) 2021 Guido Berhoerster <guido+ubports@berhoerster.name>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/ubports/infrastructure/ubports-ci-dispatcher"
)

const DefaultConfigFile = "/etc/ubports-ci-dispatcher.toml"

var (
	configFile string
	debug      bool
)

func init() {
	flag.StringVar(&configFile, "config", DefaultConfigFile,
		"Specify configuration file")
	flag.BoolVar(&debug, "debug", false, "Enable debug logging")
}

func usage() {
	fmt.Fprintf(flag.CommandLine.Output(),
		"usage: %s [options] data_file:\n", os.Args[0])
	flag.PrintDefaults()
}

func startServer() (err error) {
	c, err := cidispatcher.ReadConfig(configFile)
	if err != nil {
		return
	}

	if err = os.MkdirAll(c.IncomingPath, 0750); err != nil {
		err = fmt.Errorf("failed to create incoming directory: %w", err)
		return
	}

	logf := os.Stderr
	if c.Logfile != "" {
		logf, err = os.OpenFile(c.Logfile,
			os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0640)
		if err != nil {
			err = fmt.Errorf("failed to open log file: %w", err)
			return
		}
		defer func() {
			if cerr := logf.Close(); err == nil && cerr != nil {
				err = fmt.Errorf("failed to close log file: %w",
					cerr)
			}
		}()
	}

	l := cidispatcher.NewDefaultLogger(os.Stderr, "", log.LstdFlags, debug)

	err = cidispatcher.NewCIDispatcher(c, l).ListenAndServe()
	return
}

func main() {
	flag.Usage = usage
	flag.Parse()
	if flag.NArg() != 0 {
		usage()
		os.Exit(2)
	}

	if err := startServer(); err != nil {
		fmt.Fprintln(flag.CommandLine.Output(), err)
		os.Exit(1)
	}
}
