// Copyright (C) 2021 Guido Berhoerster <guido+ubports@berhoerster.name>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package cidispatcher

import (
	"errors"
	"context"
	"time"
)

type Publisher struct {
	publishc <-chan PublishBuildJob
	aptly    *AptlyClient
	log      Logger
	config   *Config
}

func NewPublisher(publishc <-chan PublishBuildJob, aptly *AptlyClient, l Logger, config *Config) *Publisher {
	return &Publisher{
		publishc: publishc,
		aptly:    aptly,
		log:      l,
		config:   config,
	}
}

func (p *Publisher) processJob(job PublishBuildJob) {
	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	p.log.Debugf("[%s] ensuring repo %s exists", job.ID, job.Repo)
	err := p.aptly.CreateRepo(ctx, job.Repo)
	if err != nil && !errors.Is(err, ErrRepoExists) {
		p.log.Printf("[%s] failed to create repo %s: %s",
			job.ID, job.Repo, err)
		return
	}

	p.log.Debugf("[%s] adding packages to repository %s", job.ID,
		job.Repo)
	if err := p.aptly.AddToRepo(ctx, job.Repo, job.ID); err != nil {
		p.log.Printf("[%s] failed to add packages to repository %s: %s",
			job.ID, job.Repo, err)
		return
	}

	if err := p.aptly.PublishOrUpdateRepo(ctx, job.Repo,
		p.config.GPGKey, p.config.GPGPassphraseFile); err != nil {
		p.log.Printf("[%s] failed to publish repository %s: %s",
			job.ID, job.Repo, err)
		return
	}

	p.log.Printf("[%s] successfully added packages and published repository %s",
		job.ID, job.Repo)
}

func (p *Publisher) Run() {
	for job := range p.publishc {
		p.processJob(job)
	}
}
